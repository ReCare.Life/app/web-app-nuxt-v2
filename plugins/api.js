import API from '@/utils/api'
export default function ({ $axios }, inject) {
  // Create a custom axios instance
  const axios = $axios.create({
    baseURL: process.env.API_BASE_URL,
  })
  const api = new API({
    http: axios,
  })
  
  // Inject to context as $api
  inject('api', api)
}
